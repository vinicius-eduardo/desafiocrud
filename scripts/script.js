var userTR = document.getElementById("nameTR");
var projectsArr = [];	//array com todos os projetos

UpdateStatus();

//Inicia a criação de um novo projeto
function Initialize(){
	event.preventDefault();
	Create();
	Read();
	UpdateStatus();
	document.getElementById("form").reset();
};

//atribui os valores ao objeto
function Create(){
	var name = document.getElementById("name").value;
	var dev = document.getElementById("developer").value;
	var tec = document.getElementById("tecnology").value;
	var time = document.getElementById("time").value;
	var price = document.getElementById("price").value;
	var projectOBJ = {};	//um projeto
	
	projectOBJ["name"] = name;
	projectOBJ["developer"] = dev;
	projectOBJ["tecnology"] = tec;
	projectOBJ["developmentTime"] = time;
	projectOBJ["price"] = price;

	projectsArr.push(projectOBJ);
}

//lê e imprime os objetos
function Read(){
	userTR.innerHTML = '';

	if(projectsArr != null){
		for(var i = 0; i < projectsArr.length; i++){
			userTR.innerHTML += `
			<div class="column is-8 is-offset-2">
				<div class="box">
					<div class="field">
						<p><i class = "fas fa-code"></i> Nome do Projeto: ${projectsArr[i].name}</p>
					</div>
					<div class="field">
						<p><i class = "fas fa-user"></i> Desenvolvedor(es): ${projectsArr[i].developer}</p>
					</div>
					<div class="field">
						<p><i class="fas fa-cog"></i> Tecnologia: ${projectsArr[i].tecnology}</p>
					</div>
					<div class="field">
						<p><i class="fas fa-calendar-alt"></i> Tempo de Desenvolvimento: ${projectsArr[i].developmentTime} dias</p>
					</div>
					<div class="field">
						<p><i class="fas fa-money-bill-wave"></i> Preço: R$ ${projectsArr[i].price}</p>
					</div>
					<div class="field is-grouped">
						<p class="control is-expanded">
							<button class = "button is-block is-link is-fullwidth" onclick = "Update(${i})"><i class = "fas fa-edit"></i> Editar</button>
						</p>
						<p class="control is-expanded">
							<button class = "button is-block is-danger is-fullwidth" onclick = "Delete(${i})"><i class = "far fa-trash-alt"></i> Deletar</button>
						</p>
					</div>
				</div>
			</div>
			`
		}
	}
}

//atualiza os campos no objeto e imprime temporariamente os campos
function Update(index){
	userTR.innerHTML = '';

	for(var i = 0; i < projectsArr.length; i++){
		if(i == index){
			userTR.innerHTML += `
			<div class="column is-8 is-offset-2">
				<div class="box">
					<div class="field">
						<p><i class = "fas fa-code"></i> Editando: "${projectsArr[i].name}"</p>
					</div>
					<div class="field">
						<div class="control">
							<input type="text" class = "input" id = "newName" placeholder = "${projectsArr[i].name}">
						</div>
					</div>
					<div class="field">
						<div class="control">
							<input type="text" class = "input" id = "newDev" placeholder = "${projectsArr[i].developer}">
						</div>
					</div><div class="field">
						<div class="control">
							<input type="text" class = "input" id = "newTec" placeholder = "${projectsArr[i].tecnology}">
						</div>
					</div>
					<div class="field">
						<div class="control">
							<input type="number" class = "input" id = "newTime" placeholder = "${projectsArr[i].developmentTime}">
						</div>
					</div>
					<div class="field">
						<div class="control">
							<input type="number" class = "input" id = "newPrice" placeholder = "${projectsArr[i].price}">
						</div>
					</div>
					<div class="field is-grouped">
						<p class="control is-expanded">
							<button class = "button is-block is-info is-fullwidth" onclick = "validateUpdate(${i})"><i class = "fas fa-edit"></i> Confirmar</button>
						</p>
						<p class="control is-expanded">
							<button class = "button is-block is-danger is-fullwidth" onclick = "Read()"><i class = "fas fa-ban"></i> Cancelar</button>
						</p>
					</div>
				</div>
			</div>
			`
		}
		else{
			userTR.innerHTML += `
			<div class="column is-8 is-offset-2">
				<div class="box">
					<div class="field">
						<p><i class="fas fa-code"></i> Nome do Projeto: ${projectsArr[i].name}</p>
					</div>
					<div class="field">
						<p><i class = "fas fa-user"></i> Desenvolvedor(es): ${projectsArr[i].developer}</p>
					</div>
					<div class="field">
						<p><i class="fas fa-cog"></i> Tecnologia: ${projectsArr[i].tecnology}</p>
					</div>
					<div class="field">
						<p><i class="fas fa-calendar-alt"></i> Tempo de Desenvolvimento: ${projectsArr[i].developmentTime} dias</p>
					</div>
					<div class="field">
						<p><i class="fas fa-money-bill-wave"></i> Preço: R$ ${projectsArr[i].price}</p>
					</div>
					<div class="field is-grouped">
					<p class="control is-expanded">
						<button class = "button is-block is-link is-fullwidth" onclick = "Update(${i})"><i class = "fas fa-edit"></i> Editar</button>
					</p>
					<p class="control is-expanded">
						<button class = "button is-block is-danger is-fullwidth" onclick = "Delete(${i})"><i class = "far fa-trash-alt"></i> Deletar</button>
					</p>
					</div>
				</div>
			</div>
			`
		}
	}
	UpdateStatus();
}

//verifica se algo novo foi inserido nos campos pra só então atualizar no objeto
function validateUpdate(i){
	
	if(document.getElementById("newName").value != '')
		projectsArr[i].name = document.getElementById("newName").value;
	
	if(document.getElementById("newDev").value != '')
		projectsArr[i].developer = document.getElementById("newDev").value;

	if(document.getElementById("newTec").value != '')
		projectsArr[i].tecnology = document.getElementById("newTec").value;

	if(document.getElementById("newTime").value != '')
		projectsArr[i].developmentTime = document.getElementById("newTime").value;

	if(document.getElementById("newPrice").value != '')
		projectsArr[i].price = document.getElementById("newPrice").value;

	Read();
	UpdateStatus();
}

//deleta um projeto
function Delete(index){
	projectsArr.splice(index, 1);
	Read();
	UpdateStatus();
}

//atualiza as infos do status (faturamento, projeto etc)
function UpdateStatus(){
	var receita = 0;

	for(var i = 0; i < projectsArr.length; i++){
		receita += parseFloat(projectsArr[i].price);
	}

	document.getElementById("metaProjetos").innerHTML = `${i}/ 15`;
	document.getElementById("receita").innerHTML = "R$ " + receita + " / R$ 30000";
	document.getElementById("metaReceita").innerHTML = ((receita/30000) * 100).toFixed(1) + "%";

	Farol(receita);
}

function ClearColor(){
	document.getElementById("status").classList.remove("is-light");
	document.getElementById("status").classList.remove("is-success");
	document.getElementById("status").classList.remove("is-danger");
	document.getElementById("status").classList.remove("is-warning");
};

//torna visível no header a situação do farol
function Farol(receita){
	if((receita >= 10000) & (receita <= 20000)){
		ClearColor();
		document.getElementById("status").classList.add("is-warning");
	}
	else if(receita > 20000){
		ClearColor();
		document.getElementById("status").classList.add("is-success");
	}
	else if(receita > 0 & receita < 10000){
		ClearColor();
		document.getElementById("status").classList.add("is-danger");
	}
	else{
		ClearColor();
		document.getElementById("status").classList.add("is-light");
	}
}